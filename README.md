SiLogger
-------

Расширение для логирования ошибок в Yii2.

## Установка
Выполнить команду в консоли
```bash
composer require --prefer-dist sergks/silogger
```

или добавить в composer.json
```php
"require": {
    "sergks/silogger": "@dev"
}
```

## Использование

```php
/** @var $exception \Exception */

<?= sergks\log\SiLogger::widget([
	'exception' => $exception
]) ?>
```
