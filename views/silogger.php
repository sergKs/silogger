<?php

/* @var string $url */
/* @var string $uri */
/* @var $exception Exception */

?>

<script>
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "<?= $url ?>", true);

	var data = new FormData();
	data.append("domain", "<?= Yii::$app->request->hostInfo ?>");
	data.append("url", "<?= str_replace("\\", "/", $uri) ?>");
	data.append("file", "<?= str_replace("\\", "/", $exception->getFile()) ?>");
	data.append("line", "<?= str_replace("\\", "/", $exception->getLine()) ?>");
	data.append("message", "<?= str_replace("\\", "/", $exception->getMessage()) ?>");
	data.append("stackTrace", "<?= str_replace("\n", "\\n", str_replace("\\", "/", $exception->getTraceAsString())) ?>");

	xhr.send(data);
</script>
