<?php

namespace sergks\log;

use yii\base\Widget;

class SiLogger extends Widget
{
	/**
	 * @var \Exception
	 */
	public $exception;

	/**
	 * @var string URL
	 */
	public $url = 'http://siloger.ru/api/log/add';

	/**
	 * {@inheritDoc}
	 */
	public function run()
	{
		return $this->render('silogger', [
			'url' => $this->url,
			'uri' => $_SERVER['REQUEST_URI'],
			'exception' => $this->exception
		]);
	}
}
